# H5复制插件

uni-app的h5复制插件，复制字符串到剪贴板，兼容微信、谷歌、Safari、Firefox、QQ、360等主流浏览器，试过所有h5复制插件，这个是兼容性最好的，如有不兼容的情况，请留言

## 测试地址
[https://qrk12.gitee.io/h5-copy/](https://qrk12.gitee.io/h5-copy/)


## 使用方法

```
// 引入文件
 import h5Copy from '@/js_sdk/junyi-h5-copy/junyi-h5-copy.js'
 
 export default {
 
 	methods: {
    // 触发方法
     copy() {
       let content = 'H5复制插件' // 复制内容，必须字符串，数字需要转换为字符串
       const result = h5Copy(content)
       if (result === true) {
         uni.showToast({
           title:'复制成功',
         })
       } else {
         uni.showToast({
           title:'复制失败',
           icon:'none'
         })
       }
       
     }
 	}
 }
```